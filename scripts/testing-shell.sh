#!/bin/bash                                                                               

temp_version=()
temp_env=()
for directory in `find . -mindepth 5  -type d`
do
  if [[ $directory =~ ^./([^/]*)/([^/]*)/([^/]*)/([^/]*)/([^/]*) ]]; then
    temp_version+=( ${BASH_REMATCH[3]} )
    temp_env+=( ${BASH_REMATCH[5]} )
  fi
done


version=( $(echo ${temp_version[@]}|tr ' ' '\n'|sort -u ))
env=( $(echo ${temp_env[@]}|tr ' ' '\n'|sort -u) )

# version=( 1.0 2.0 )
# env=(dev uat prod)
# string=/home/jcdusse/etc

# if [[ "$string" =~ ^/([^/]*)/([^/]*)/([^/]*) ]]; then
  # echo "${BASH_REMATCH[1]}"
# fi

cat <<- EOF2
stages:
  - version
  - env

variables:
  WHO: \${WHO}

env:
  stage: env
  script:
    - export

EOF2


for ver in ${version[@]};
do
if [[ "$ver" =~ ([^/]*)/([^/]*) ]]; then
  version=${BASH_REMATCH[1]}
  env=${BASH_REMATCH[2]}
fi
cat <<- EOF
"${ver}":
  stage: version
  # tags:
    # - "local-k8s-master-1804"
  script:

    - echo version=$version
    - echo WHOAMI=\${WHOAMI}
    - echo P_RELEASE=${P_RELEASE}
    - echo WHO=\${WHO}
    - echo MESSAGE=\${MESSAGE}
    - echo VAR1=\${VAR1}
    - echo CI_PIPELINE_IID=${CI_PIPELINE_IID}
    - echo AWESOME_PIPELINE_IID=${AWESOME_PIPELINE_IID}
    - echo TRIGGER_CHILD_PIPE_IID=${TRIGGER_CHILD_PIPE_IID}
    - echo PIPELINE_BUILD_PIPE_IID=\${PIPELINE_BUILD_PIPE_IID}

EOF
done
